#!/bin/bash

# See: https://extranet.atlassian.com/display/BBCI/questions/2662794344/how-can-i-build-a-java-project-in-bitbucket-ci-that-relies-on-closedsource-parent-pom

mvn --version

MAVEN_SETTINGS="$(mvn --version | sed -n -E 's/Maven home: (.*)/\1/p')/conf/settings.xml"

if [ ! -f "$MAVEN_SETTINGS" ]; then
   echo "Maven not installed, or maven settings is in a different place on this docker image! Not found at '$MAVEN_SETTINGS'"
   exit 1
fi

echo "Updating configuration in Maven settings file: '$MAVEN_SETTINGS'"

sed -i'back' "/<servers>/ a\
<server><id>maven-atlassian-com</id><username>${atlassian_private_username}</username><password>${atlassian_private_password}</password></server><server><id>atlassian-public</id><username>${atlassian_private_username}</username><password>${atlassian_private_password}</password></server>" $MAVEN_SETTINGS

sed -i'bak' '/<profiles>/ a\
<profile><id>atlassian-private</id><activation><activeByDefault>true</activeByDefault></activation><repositories><repository><id>maven-atlassian-com</id><url>https://packages.atlassian.com/maven/repository/internal</url><snapshots><enabled>true</enabled><updatePolicy>never</updatePolicy><checksumPolicy>fail</checksumPolicy></snapshots><releases><enabled>true</enabled><checksumPolicy>fail</checksumPolicy></releases></repository></repositories><pluginRepositories><pluginRepository><id>maven-atlassian-com</id><url>https://packages.atlassian.com/maven/repository/internal</url><releases><checksumPolicy>fail</checksumPolicy><enabled>true</enabled></releases><snapshots><checksumPolicy>fail</checksumPolicy></snapshots></pluginRepository></pluginRepositories></profile>' $MAVEN_SETTINGS

sed -i'bak' '/<profiles>/ a\
<profile><id>atlassian-public</id><activation><activeByDefault>true</activeByDefault></activation><repositories><repository><id>atlassian-public</id><url>https://packages.atlassian.com/maven/repository/public</url></repository></repositories></profile>' $MAVEN_SETTINGS