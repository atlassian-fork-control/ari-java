package com.atlassian.ari;

import java.util.Optional;

class ARIFilterParser extends BaseARIParser<ARIFilter> {

    static ARIFilter parse(String value){
        return new ARIFilterParser().parseInternal(value);
    }

    @Override
    protected ARIFilter build(String resourceOwner, String cloudId, String resourceType, String resourceId) {
        return ImmutableARIFilter.builder()
                .resourceOwner(resourceOwner)
                .cloudId(Optional.ofNullable(cloudId))
                .resourceType(Optional.ofNullable(resourceType))
                .resourceId(Optional.ofNullable(resourceId))
                .build();
    }

    static Optional<ARIFilter> tryParse(String value) {
        try {
            return Optional.of(parse(value));
        } catch (Exception e) {
            return Optional.empty();
        }
    }

    @Override
    protected ARIPrefix getPrefix() {
        return ARIPrefix.FILTER;
    }
}
