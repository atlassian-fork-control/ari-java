package com.atlassian.ari;

import org.jetbrains.annotations.NotNull;

public enum ARIPrefix {

    CLOUD("ari:cloud:"), FILTER("ari:filter:");

    private final String value;

    ARIPrefix(String value) {

        this.value = value;
    }

    @NotNull
    public String value() {
        return value;
    }
}
