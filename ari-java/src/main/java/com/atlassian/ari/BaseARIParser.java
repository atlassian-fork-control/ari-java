package com.atlassian.ari;


abstract class BaseARIParser<T extends BaseARI> {

    T parseInternal(String value) {
        char ch;
        String ariPrefix = getPrefix().value();
        if (!value.startsWith(ariPrefix)) {
            throw new IllegalArgumentException("Invalid " + ariPrefix);
        }

        String resourceOwner = null;
        String cloudId = null;
        String resourceType = null;
        String resourceId = null;
        StringBuilder sb = new StringBuilder();
        int tokenIndex = 0;
        for (int i = ariPrefix.length(); i < value.length(); i++) {
            ch = value.charAt(i);
            if ((ch == ':' || (tokenIndex == 2 && ch == '/')) && tokenIndex < 3) {
                // new token split
                switch (tokenIndex) {
                    case 0:
                        resourceOwner = sb.toString();
                        break;
                    case 1:
                        if (sb.length() > 0) {
                            cloudId = sb.toString();
                        }
                        break;
                    case 2:
                        if (sb.length() > 0) {
                            resourceType = sb.toString();
                        }
                        break;
                }
                sb.setLength(0);
                tokenIndex++;
            } else {
                sb.append(ch);
            }
        }

        if (tokenIndex == 3) {
            if (sb.length() > 0) {
                resourceId = sb.toString();
            }
            tokenIndex++;
        }

        if (tokenIndex != 4) {
            throw new IllegalArgumentException("Invalid " + ariPrefix);
        }

        return build(resourceOwner, cloudId, resourceType, resourceId);
    }

    protected abstract T build(String resourceOwner, String cloudId, String resourceType, String resourceId);

    protected abstract ARIPrefix getPrefix();

}
