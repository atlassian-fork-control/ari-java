package com.atlassian.ari;

import java.util.Optional;

class ARIParser extends BaseARIParser<ARI> {

    static ARI parse(String value){
        return new ARIParser().parseInternal(value);
    }

    @Override
    protected ARI build(String resourceOwner, String cloudId, String resourceType, String resourceId) {
        return ImmutableARI.builder()
                .resourceOwner(resourceOwner)
                .cloudId(Optional.ofNullable(cloudId))
                .resourceType(Optional.ofNullable(resourceType))
                .resourceId(Optional.ofNullable(resourceId))
                .build();
    }

    static Optional<ARI> tryParse(String value) {
        try {
            return Optional.of(parse(value));
        } catch (Exception e) {
            return Optional.empty();
        }
    }

    @Override
    protected ARIPrefix getPrefix() {
        return ARIPrefix.CLOUD;
    }
}
