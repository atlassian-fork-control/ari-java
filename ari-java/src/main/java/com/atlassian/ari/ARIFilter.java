package com.atlassian.ari;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;
import org.immutables.value.Value;
import org.jetbrains.annotations.NotNull;

import java.util.Optional;

@Value.Immutable
public abstract class ARIFilter extends BaseARI {

    @JsonCreator
    @NotNull
    public static ARIFilter parse(@NotNull String s) {
        return ARIFilterParser.parse(s);
    }

    @NotNull
    public static Optional<ARIFilter> tryParse(@NotNull String value) {
        return ARIFilterParser.tryParse(value);
    }

    @Override
    @NotNull
    protected ARIPrefix getPrefix() {
        return ARIPrefix.FILTER;
    }

    @JsonValue
    @Value.Lazy
    @Override
    @NotNull
    public String value() {
        return super.value();
    }
}
