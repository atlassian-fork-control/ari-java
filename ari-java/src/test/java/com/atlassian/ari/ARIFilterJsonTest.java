package com.atlassian.ari;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import org.immutables.value.Value;
import org.junit.Test;

import java.io.IOException;

import static com.fasterxml.jackson.annotation.JsonInclude.Include.NON_NULL;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

public class ARIFilterJsonTest {

    @Test
    public void shouldBeParsedInsidePOJOs() throws IOException {
        FilterPOJO cloudPojo = new ObjectMapper().readValue("{\"id\": \"ari:filter:platform:+:site/+\", \"text\": \"foo\"}", FilterPOJO.class);
        ARIFilter ari = cloudPojo.id();

        assertThat(ari.getResourceOwner(), is("platform"));
        assertTrue(ari.getCloudId().isPresent());
        assertTrue(ari.getResourceType().isPresent());
        assertTrue(ari.getResourceId().isPresent());
    }

    @Test
    public void valueShouldBeInRightFormat() {
        assertThat(ARIFilter.parse("ari:filter:platform:+:site/+").value(), is("ari:filter:platform:+:site/+"));
    }

    @Value.Immutable
    @JsonSerialize(as = ImmutableFilterPOJO.class)
    @JsonDeserialize(as = ImmutableFilterPOJO.class)
    @JsonInclude(NON_NULL)
    interface FilterPOJO {
        ARIFilter id();

        String text();
    }

}
