package com.atlassian.ari;

import org.openjdk.jmh.annotations.Benchmark;

public class ARIParserBenchmark {

    @Benchmark
    public void parseCloudWithRegex() {
        ARIParser.parse("ari:cloud:confluence:123:comment/1");
    }

    @Benchmark
    public void parseFilterWithRegex() {
        ARIFilterParser.parse("ari:filter:hipchat.cloud:*:resource/*");
    }

}
