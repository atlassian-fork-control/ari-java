package com.atlassian.ari;

import static com.fasterxml.jackson.annotation.JsonInclude.Include.NON_NULL;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import java.io.IOException;

import org.immutables.value.Value;
import org.junit.Test;

public class ARIJsonTest {

    @Test
    public void shouldBeParsedInsidePOJOs() throws IOException {
        CloudPOJO cloudPojo = new ObjectMapper().readValue("{\"id\": \"ari:cloud:confluence:123:comment/1\", \"text\": \"foo\"}", CloudPOJO.class);
        ARI ari = cloudPojo.id();

        assertThat(ari.getResourceOwner(), is("confluence"));
        assertTrue(ari.getCloudId().isPresent());
        assertTrue(ari.getResourceType().isPresent());
        assertTrue(ari.getResourceId().isPresent());
    }

    @Test
    public void valueShouldBeInRightFormat() {
        assertThat(ARI.parse("ari:cloud:confluence:123:comment/1").value(), is("ari:cloud:confluence:123:comment/1"));
    }

    @Value.Immutable
    @JsonSerialize(as = ImmutableCloudPOJO.class)
    @JsonDeserialize(as = ImmutableCloudPOJO.class)
    @JsonInclude(NON_NULL)
    interface CloudPOJO {
        ARI id();

        String text();
    }

}
