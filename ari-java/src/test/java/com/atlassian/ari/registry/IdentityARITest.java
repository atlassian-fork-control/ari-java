package com.atlassian.ari.registry;

import org.junit.Test;

import static com.atlassian.ari.registry.IdentityARI.allPrincipals;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.junit.Assert.assertThat;

public class IdentityARITest {

    @Test
    public void allPrincipalsShouldBeValid() {
        assertThat(allPrincipals().value(), is(notNullValue()));
    }
}