package com.atlassian.ari;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

public class ARIFilterParserTest {
    @Rule
    public ExpectedException expectedExceptionRule = ExpectedException.none();

    @Test
    public void shouldParseValidARI() {
        ARIFilter ari = ARIFilterParser.parse("ari:filter:jira:321:site/123");
        assertThat(ari.getCloudId().get(), is("321"));
        assertThat(ari.getResourceOwner(), is("jira"));
        assertThat(ari.getResourceType().get(), is("site"));
        assertThat(ari.getResourceId().get(), is("123"));
    }

    @Test
    public void shouldParseValidARIWithoutCloudId() {
        ARIFilter ari = ARIFilterParser.parse("ari:filter:+::site/+");
        assertThat(ari.getCloudId(), is(Optional.empty()));
        assertThat(ari.getResourceOwner(), is("+"));
        assertThat(ari.getResourceType().get(), is("site"));
        assertThat(ari.getResourceId().get(), is("+"));
    }

    @Test
    public void shouldParseValidARIWithPlusResourceId() {
        ARIFilter ari = ARIFilterParser.parse("ari:filter:platform:321:site/+");
        assertThat(ari.getCloudId().get(), is("321"));
        assertThat(ari.getResourceOwner(), is("platform"));
        assertThat(ari.getResourceType().get(), is("site"));
        assertThat(ari.getResourceId().get(), is("+"));
    }

    @Test
    public void shouldParseValidARIWithoutCloudIdAndPlusAsResourceId() {
        ARIFilter ari = ARIFilterParser.parse("ari:filter:jira::site/+");
        assertThat(ari.getCloudId(), is(Optional.empty()));
        assertThat(ari.getResourceOwner(), is("jira"));
        assertThat(ari.getResourceType().get(), is("site"));
        assertThat(ari.getResourceId().get(), is("+"));
    }

    @Test
    public void shouldParseValidARIWithWildcardAsResourceId() {
        ARIFilter ari = ARIFilterParser.parse("ari:filter:hipchat.cloud:123:resource/*");
        assertThat(ari.getCloudId().get(), is("123"));
        assertThat(ari.getResourceOwner(), is("hipchat.cloud"));
        assertThat(ari.getResourceType().get(), is("resource"));
        assertThat(ari.getResourceId().get(), is("*"));
    }

    @Test
    public void shouldParseValidARIWithWildcardAsCloudIdAndResourceId() {
        ARIFilter ari = ARIFilterParser.parse("ari:filter:hipchat.cloud:*:resource/*");
        assertThat(ari.getCloudId().get(), is("*"));
        assertThat(ari.getResourceOwner(), is("hipchat.cloud"));
        assertThat(ari.getResourceType().get(), is("resource"));
        assertThat(ari.getResourceId().get(), is("*"));
    }

    @Test
    public void shouldParseValidARIWithWildcardAsCloudIdAndPlusAsResourceId() {
        ARIFilter ari = ARIFilterParser.parse("ari:filter:hipchat.cloud:*:resource/+");
        assertThat(ari.getCloudId().get(), is("*"));
        assertThat(ari.getResourceOwner(), is("hipchat.cloud"));
        assertThat(ari.getResourceType().get(), is("resource"));
        assertThat(ari.getResourceId().get(), is("+"));
    }

    @Test
    public void shouldParseValidARIWithEmptyResourceId() {
        ARIFilter ari = ARIFilterParser.parse("ari:filter:hipchat.cloud:123:conversation/");
        assertThat(ari.getCloudId().get(), is("123"));
        assertThat(ari.getResourceOwner(), is("hipchat.cloud"));
        assertThat(ari.getResourceType().get(), is("conversation"));
        assertThat(ari.getResourceId(), is(Optional.empty()));
    }

    @Test
    public void shouldThrowForInvalidARI() {
        expectedExceptionRule.expect(IllegalArgumentException.class);
        ARIFilterParser.parse("fakeAri");
    }

    @Test
    public void shouldThrowForInvalidARI2() {
        expectedExceptionRule.expect(IllegalArgumentException.class);
        ARIFilterParser.parse("ari:filter:bla");
    }

    @Test
    public void shouldReturnEmptyForInvalidARI() throws Exception {
        assertThat(ARIFilterParser.tryParse("fakeAri"), is(Optional.empty()));
        assertThat(ARIFilter.tryParse("fakeAri"), is(Optional.empty()));

    }

}