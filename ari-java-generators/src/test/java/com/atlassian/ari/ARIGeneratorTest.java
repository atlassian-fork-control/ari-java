package com.atlassian.ari;

import com.pholser.junit.quickcheck.Property;
import com.pholser.junit.quickcheck.runner.JUnitQuickcheck;
import org.junit.runner.RunWith;

@RunWith(JUnitQuickcheck.class)
public class ARIGeneratorTest {
    @Property
    public void testWeCanGenARIs(ARI ari) {
        // We don't need to do anything here - we just need to know the classes are loaded correctly etc.
    }
}
